# [Migrate](https://www.npmjs.com/package/@sichangi/migrate)
[![pipeline status](https://gitlab.com/mecolela/migrate/badges/master/pipeline.svg)](https://gitlab.com/mecolela/migrate/commits/master)
> A database migration library for mongodb via mongoose

Run mongodb migrations via the CLI or programatically using mongoose models

### Getting Started

Install from npm / yarn
```
 yarn add @sichangi/migrate
```

### Usage
```
migrate -h
```

##### Commands:
```bash
migrate list [options]

migrate create <migration_name> [options]

migrate up <migration_name> [options]

migrate down <migration_name> [options]

migrate prune [options]
```

#####  Config  File

The config file source path defaults to ``./migrate.[json|js]``

```json
{
  "autoSync": false,
  "migrationsDir": "./migrations",
  "collectionName": "migrations",
  "dbConnectionUri": "mongodb+srv://username:password@mongodb.example.com/database",
}
```

### Migration Files

Here's an example of a migration created using `migrate create some-migration-name` . 

###### migrations/1562460744403-some-migration-name.js
```javascript
/**
 * Make any changes you need to make to the database here
 */
async function up () {
  // Write migration here
}

/**
 * Make any changes that UNDO the up function;s operations here
 */
async function down () {
  // Write migration here
}

module.exports = { up, down };
```

#### Sample setup

Below is an example of a typical setup in a mongoose project

##### models/user.model.js
```javascript
const {Schema} = require('mongoose');

const UserSchema = new Schema({
    firstName: String,
    lastName: String,
  });

module.exports = mongoose.model('user', UserSchema);
```


##### models/index.js
```javascript
const mongoose = require('mongoose');
const User = require('./user.model');

mongoose.connect('mongodb://localhost:27017/mydb', { useNewUrlParser: true })

module.exports = { User };
```

##### migrations/1459287720919-my-migration.js
```javascript
import { User } from '../models'

async function up() {
  // Then you can use it in the migration like so  
  await User.create({ firstName: 'Ada', lastName: 'Lovelace' });
}
```


