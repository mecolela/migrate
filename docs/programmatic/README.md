### Programmatic usage

```js
var Migrator = require('@sichangi/migrate');

let migrator = new Migrator({
  migrationsPath:  '/path/to/migrations', // Path to migrations directory
  templatePath: null, // The template to use when creating migrations needs up and down functions exposed
  dbConnectionUri: 'mongodb://localhost/db', // mongo url
  collectionName:  'sample', // collection name to use for migrations (defaults to 'migrations')
  autosync: true // if making a CLI app, set this to false to prompt the user, otherwise true
});


var migrationName = 'myNewMigration', promise;

// Create a new migration
migrator.create(migrationName).then(()=> {
  console.log(`Migration created. Run `+ `migrate up ${migrationName}`.cyan + ` to apply the migration.`);
});

// Migrate Up
promise = migrator.run('up', migrationName);

// Migrate Down
promise = migrator.run('down', migrationName);

// List Migrations
/*
Example return val

Promise which resolves with
[
 { name: 'my-migration', filename: '149213223424_my-migration.js', state: 'up' },
 { name: 'add-cows', filename: '149213223453_add-cows.js', state: 'down' }
]
*/
promise = migrator.list();


// Prune extraneous migrations from file system
promise = migrator.prune();

// Synchronize DB with latest migrations from file system
/*
Looks at the file system migrations and imports any migrations that are
on the file system but missing in the database into the database

This functionality is opposite of prune()
*/
promise = migrator.sync();


