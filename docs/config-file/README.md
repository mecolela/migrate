#### Config file

> `migrate.json` by default

```
$ migrate create my_new_migration
```



You can change the name of the config file to expect by providing the `config` option e.g. `--config some_other_config_file_name.json`. Note that this file has to be a valid JSON file.

**Override Order:**

1. CLI args
2. Config file
3. ENV variables

