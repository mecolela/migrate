### CLI

### Creating a Migration

Setup a new migration

```
$ migrate  create <migration name> [options: -d mongodb://...]
```

### Listing Migrations

Shows you the migrations with their current states.

- **UP** -  the migration has already been run
- **DOWN** - the migration has not been run yet

```
$ migrate list [options]
```

Assuming `migrate list` command shows

```
UP:  	  1450107140857-user-credit-to-vault.js
UP:  	  1452541801404-user-default-billing-to-default-billing-incoming.js
UP:  	  1461351953091-add_inventory.js
DOWN:	  1463003345598-add_processed_credit_cards.js
DOWN:  	  1463603842010-add_default_regional_settings.js
```

This means the first 3 migrations ran. the next 2 need to run to be all up to date with the latest schema/data changes.


### Running a Migration (Migrate UP)

```
$ migrate [options] up add_default_regional_settings
```

To migrate **UP TO (and including)**  `1463603842010-add_default_regional_settings.js`

Your new state will be

```
UP:  	  1450107140857-user-credit-to-vault.js
UP:  	  1452541801404-user-default-billing-to-default-billing-incoming.js
UP:  	  1461351953091-add_inventory.js
UP:	      1463003345598-add_processed_credit_cards.js
UP:  	  1463603842010-add_default_regional_settings.js
```

### Undoing Migrations (Migrate DOWN)

What if you want to undo the previous step?

Simply run

```
$ migrate [options] down add_processed_credit_cards
```

and you'll migrate **DOWN TO (and including)** `1463003345598-add_processed_credit_cards.js`

Your new state will be 

```
UP:  	  1450107140857-user-credit-to-vault.js
UP:  	  1452541801404-user-default-billing-to-default-billing-incoming.js
UP:  	  1461351953091-add_inventory.js
DOWN:	  1463003345598-add_processed_credit_cards.js
DOWN:  	  1463603842010-add_default_regional_settings.js
```


##### Synchronizing Your DB with new Migrations

Lets say you `git pull` the latest changes from your project and someone had made a new migration called `add_unicorns`.

When you go run any migration command (e.g. `migrate list`), you are prompted with
 
```
Synchronizing database with file system migrations...
? The following migrations exist in the migrations folder but not in the database. Select the ones you want to import into the database (Press <space> to select)
❯◯ 1463003339853-add_unicorns.js
```
This is telling you that someone added a migration file that's your database doesn't have yet.
If you select it by pressing **Space** then **Enter** on your keyboard, you can tell `@sichangi/migrate` to import it into the database.

Once imported, the default state is DOWN, so you'll have to `migrate up add_unicorns` to be all up-to-date.

**IF ON THE OTHER HAND** you don't want this migration, simply run 

```
$ migrate prune
```

and you'll be prompted to remove it from the **FILE SYSTEM**.

##### But what happens if I want to sync automatically instead of doing this every time?

just add the option `--autosync` and the migrate command will automatically import new migrations in your migrations folder before running commands.
